pipeline{
    agent any
    tools {
        maven 'maven-3.8'
    }
    stages{
        stage("increment version"){
            steps{
                script{
                    echo "Incrementing app version..."
                    sh 'mvn build-helper:parse-version versions:set \
                     -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                     versions:commit'
                     def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                     def version = matcher[0][1]
                     env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }

        stage("build app"){
            steps{
                script {
                    echo "Building a jar artifact..."
                    sh 'mvn clean package'
                }
            }
        }

        stage("build and push image"){
            steps{
                script {
                    echo "Building the Docker image..."
                        withCredentials([usernamePassword(credentialsId: 'dockerhub-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                            sh "docker build -t tristo/java-maven-app:${IMAGE_NAME} ."
                            sh "echo $PASS | docker login -u $USER --password-stdin"
                            sh "docker push tristo/java-maven-app:${IMAGE_NAME}"
                        }
                }
            }
        }

        stage("deploy"){
            steps{
                script {
                    echo 'Deploying Docker image to EC2...'
                }
            }
        }

        stage("commit version update"){
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/Tristo/java-maven-app.git"
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:master'
                    }
                }
            }

        }
    }
}
